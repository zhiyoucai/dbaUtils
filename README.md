# dbaUtils

#### 介绍
用Go语言开发的一些日用小工具

#### 环境
我是用Go1.12.5编写的

#### 使用方式
编译成可执行完文件即可使用，命令行参数示范：

```bash
binlogPrinter.exe -ServerID=1 -Flavor=mysql -Host=127.0.0.1 -Port=3306 -User=root -Password=Zhiquan2019_ -binlog="binlog.000001" -
pos=4 -logger="d:\\index.log"
```
