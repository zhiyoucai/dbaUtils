package libs

import "log"

func CheckErrors(msg string, err error) {
	if err != nil {
		log.Panic(msg, err)
	}
}